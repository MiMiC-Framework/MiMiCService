#ifndef MSERVICE_ERROR_H
#define MSERVICE_ERROR_H

#if (defined _WIN32)
#include <windows.h>
#elif (defined __linux)
#include <stdint.h>
#endif

#include <UIHandler/Error.h>

#include <uerrno/uerrno.h>

#include <string>

extern "C" {

/**
 * @brief
 *  Struct to describe the error message codes of the Core.
 * @details
 */
struct _cErrorDesc {
    int  code;
    char *message;
};

struct ParseError {
    errno_t code;
    std::string msg;
};

}

std::string toString(error_t err);

#endif
