#ifndef M_UISERVICE_H
#define M_UISERVICE_H

#include <UErrno/uerrno.h>
#include <MService/LogServ/LogService.h>
#include <MService/Payloads/AppUISettings.h>
#include <FUtils/Sync/SRVar.h>

/**
 * @brief
 *  Starts the UI Service.
 * @details
 *  The UI Service is responsable for receiving the payloads and working.
 * @param uiEventQueuePs
 *  The queue of UI Events that are going to be received encapsulated in the
 *  necessary primitives to handle it's synchronous access.
 * @param uiElemActionQueuePs
 *  The queue of Actions over UI elements that are going to be received encapsulated in the
 *  necessary primitives to handle it's synchronous access.
 * @param sLogQueue
 *  The queue for pushing information to logging thread.
 * @param sStopFlag
 *  A flag to stop the service.
 * @param uiServiceThread
 *  The thread representing the Service.
 * @return
 */
errno_t startUIService(
    /* In  */ SyncRsc<UIEventQueue>&       uiEventQueue,
    /* In  */ SyncRsc<AppUISettingsQueue>& uiAppSettingsQueue,
    /* Out */ SVar<LogQueue>&              sLogQueue,
    /* In  */ SVar<bool>&                  sStopFlag,
    /* In  */ std::thread&                 uiServiceThread
);

#endif
