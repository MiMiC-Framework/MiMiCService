#ifndef MCS_REQUESTPARSING_H
#define MCS_REQUESTPARSING_H

#include <json.hpp>

#include <MService/Payloads/Request.h>
#include <MService/Payloads/AppUISettings.h>
#include <MService/Error.h>

#include <FUtils/Ret/Either.h>
#include <Std/Vector.h>

void replace(std::string& str, std::string& subStr);
Either<Vector<Request>, ParseError> parseRequest(std::string requests);

#endif
