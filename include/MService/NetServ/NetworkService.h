/*
 * Copyright 2017 RtF-US
 *
 * Licensed under the New BSD license. You may not use this file except in
 * compliance with this License.
 *
 * You may obtain a copy of the License at
 * https://gitlab.com/MiMiC/MiMiCService/LICENSE.txt
 */
/**
 * @file   NetworkService.h
 * @author Javier Jaramago Fernández(jj@raisingthefloor.org)
 * @date   October, 2017
 * @brief  Main file of the NetworkService for the MiMiC Service.
 * @details
 * The NetworkService is responsible of:
 *     - Accepting and parsing the requests.
 *     - Notifying clients when a request have been received, if it was accepted
 *       or declined and why.
 *     - Queueing the accepted requests for the ProcService to handle them.
 */

#ifndef MS_NETSERVICE_H
#define MS_NETSERVICE_H

/**
 * @defgroup AsioDefinitions
 * @brief Definitions for Asio networking library support in windows.
 * @details
 *  The following definitions should be placed in front of Asio and Windows
 *  inclusion headers.
 */

/** @ingroup AsioDefinitions */
#define ASIO_STANDALONE
/** @ingroup AsioDefinitions */
#define ASIO_HAS_STD_ADDRESSOF
/** @ingroup AsioDefinitions */
#define ASIO_HAS_STD_ARRAY
/** @ingroup AsioDefinitions */
#define ASIO_HAS_CSTDINT
/** @ingroup AsioDefinitions */
#define ASIO_HAS_STD_SHARED_PTR
/** @ingroup AsioDefinitions */
#define ASIO_HAS_STD_TYPE_TRAITS

// ============================ DEPENDENT =====================================
#include <asio.hpp>
#include <Ole2.h>
// ============================================================================

#include <MService/LogServ/LogService.h>
#include <MService/Payloads/Request.h>

#include <FUtils/Ret/Either.h>
#include <FUtils/Sync/Svar.h>

#include <json.hpp>
#include <stdint.h>

/**
 * @brief
 *  Initializes the network service.
 * @param sReqQueue
 *  The queue in which the new received operations are going to be queued.
 * @param sLogQueue
 *  The queue which is going to be filled with the debugging information for the logging service.
 * @param sStopFlag
 *  Flag holded inside a SVar to advice when the service should be stopped.
 * @param netServiceThread
 *  Thread representing the network service.
 * @param port
 *  If NULL, a error is returned if port passed in first parameter is not available.
 *  If not NULL, a random port will be selected, until finding one that is available,
 *  that selected port will be returned in this parameter.
 * @return
 *  E_FN_SUCESS or one of the following errors:
 *  **TODO**
 */
errno_t startNetService(
    uint16_t                cport,
    SVar<RequestQueue>&     sReqQueue,
    SVar<LogQueue>&         sLogQueue,
    SVar<bool>&             sStopFlag,
    std::thread*            netServThread,
    uint16_t*               port = NULL
);

#endif // !MS_NETSERVICE_H
