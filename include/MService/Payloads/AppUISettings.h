#ifndef MS__UITYPES_H
#define MS__UITYPES_H

#include <UIHandler/UI/UITypes.h>
#include <CVariant/Variant.h>
#include <Std/Vector.h>

#include <json.hpp>
#include <UIAutomation.h>

#include <queue>

using json = nlohmann::json;

// UIAction related functions
// ============================================================================

std::string toString(UIElemActionCode code);
UIElemActionCode parseUIElemActionCode(std::string s);

using UIElemActionQueue = std::queue<UIElemAction>;

bool operator==(const UIElemAction& uiElemA1, const UIElemAction& uiElemA2);
// ============================================================================

/**
 * @brief
 * Enum with the possible modes a operation could be called.
 *     - If mode is 'Direct' the specified settings will be called without waiting
 *       and if they are not available, they wont be triggered.
 *     - If mode is 'Event' the specified settings will try to be called in the
 *       specified applications, if they are not available, events will be
 *       created, waiting for then to be available.
 */
enum SettingExMode {
    SetExMode_Direct  = 0,
    SetExMode_Event   = 1,
    SetExMode_ENODEF  = 2
};

std::string toString(SettingExMode exMode);

/**
 * @struct  UIAction
 * @brief
 *  Struct with the necessary information for executing an action over an UI Element.
 * @var UIAction::appInfo
 *  Information for identifying the app UI in which we want to identify the action.
 * @var UIAction::elemId
 *  Unique identifier for the element in which the action is going to be executed.
 * @var UIAction::elemAction
 *  The action that is going to be executed over the element with id 'elemId'.
 */
struct UIAction {
    uint64_t        appId;
    int64_t         eventId;
    std::wstring    elemId;
    UIElemAction    elemAction;
};

bool operator==(const UIAction& appUIElemA1, const UIAction& appUIElemA2);
void from_json(const json& j, UIAction& action);
void from_json(const json &j, Vector<UIAction>& uiActions);
void to_json(json& j, const UIAction& action);

using AppUIElemActionQueue = std::queue<UIAction>;

// using Setting = std::pair<std::string, std::list<ElemIdAction>>;

using std::optional;

/**
 * @brief
 * Mode in which the application can be, it holds:
 *     - A name for the mode.
 *     - An identifier of the mode.
 *     - A list of settings for the modes.
 * @var name The UI mode name, human friendly string to identify the mode.
 * @var modeId The mode "id" is a string that can only be found in that app mode.
 * @var settings The list of settings that can be triggered from that UI Mode.
 */
struct AppUIMode {
    std::string name;
    std::string id;
};

/**
 * @brief operator == For two AppUIMode structs.
 * @param appMode1 First mode for eq comparison.
 * @param appMode2 Second mode for eq comparison.
 * @return True if both AppUIMode are the same.
 */
bool operator==(const AppUIMode& appMode1, const AppUIMode& appMode2);

/**
 * @struct Setting
 * @brief
 *  Struct for encoding a setting for an Application.
 * @var Setting::appId
 *  The id for identifying the application in the registered applications list.
 * @var Setting::name
 *  The name of the setting.
 * @var Setting::exMode
 *  The mode for the setting, if it's 'event' a event will be registered, for being the
 *  actions will be triggered.
 * @var Setting::uiMode
 *  Optional value representing the uiMode in which the setting should be triggered.
 * @var Setting::actions
 *  Set of actions to be executed over the application.
 */
struct Setting {
    uint64_t                appId;
    std::string             name;
    SettingExMode           exMode;
    optional<AppUIMode>     uiMode;
    std::vector<UIAction>   actions;
};

bool operator==(const Setting& setting1, const Setting& setting2);
std::ostream& operator<<(std::ostream& os, const Setting& obj);
void from_json(const json& j, Setting& setting);
void to_json(json& j, const Setting& setting);
void to_json(json& j, const Vector<Setting>& settings);

/**
 * @struct AppUISettings
 * @brief
 *  This includes information about:
 *      - The application.
 *      - The list of modes the applications have.
 *      - The list of settings each mode needs.
 * @var AppUISettings::appInfo
 *  The information to fully identify the application.
 * @var AppUISettings::uiModes
 *  The list of UI modes in which an application can be.
 */
struct AppUISettings {
    AppInfo             appInfo;
    Vector<Setting>     settings;
};

bool operator==(const AppUISettings& app1, const AppUISettings& app2);

using AppUISettingsQueue = std::queue<AppUISettings>;
void from_json(const json& j, AppUISettings& uiSettings);
void to_json(json& j, const AppUISettings& uiSettings);

using UIEventQueue = std::queue<UIEvent>;

#endif
