Welcome to MiMiC Documentation!
===============================

.. toctree::
    :hidden:

.. page_index.rst

.. rubric:: Related Pages:
.. toctree::
    :maxdepth: 2

    README

.. rubric:: Reference and Index:

.. rubric:: System Overview:

The following schema represents the global system components interaction.
MiMiC Service is a service that exposes a REST API throught a networking
endpoint, each of the petitions made is translated into actions or events that
are executed/registered in the system UI.

The system doesn't present any kind of memory, or the actions are done or mapped
during system runtime, and lost one the service is ended.

.. image:: _static/MiMiCService.svg

.. rubric:: DataType relationships:

Datatype relationships and it's interactions are represented in the following model.

.. image:: _static/MiMiCDataTypeMode.svg

.. toctree::
    :maxdepth: 2

    Setting

.. toctree::
    :hidden:

    global.rst

.. toctree::
    :caption: Services
    :maxdepth: 2

    Network Service
    Logging Service
    UI Service

.. toctree::
    :caption: Utils 
    :maxdepth: 2

    SVar
    WMutex
    Lock

    :doc:`global`
    :ref:`genindex`
