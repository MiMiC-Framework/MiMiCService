#include <MService/NetServ/NetworkService.h>

#include <gtest/gtest.h>

TEST(NetworkService, StartStopService) {
    SVar<bool>          sStopFlag { false };
    SVar<LogQueue>      sLogQueue { LogQueue {} };
    SVar<RequestQueue>  sReqQueue { RequestQueue{} };
    std::thread         netServThread;
    uint16_t            port;

    auto netStartRes =
        startNetService(
            31400,
            sReqQueue,
            sLogQueue,
            sStopFlag,
            &netServThread,
            &port
        );

    putSVar(sStopFlag, true);
    netServThread.join();
}

TEST(NetworkService, StartAndSendStopPayload) {
    SVar<bool>          sStopFlag { false };
    SVar<LogQueue>      sLogQueue { LogQueue {} };
    SVar<RequestQueue>  sReqQueue { RequestQueue{} };
    std::thread         netServThread;
    uint16_t            port;

    // auto netStartRes =
    //     startNetService(
    //         31400,
    //         sReqQueue,
    //         sLogQueue,
    //         sStopFlag,
    //         &netServThread,
    //         &port
    //     );
//
    // std::thread client([]() {
    //     asio::io_service io_service;
    //     asio::error_code endpoint_err;
//
    // });
//
    // putSVar(sStopFlag, true);
    // netServThread.join();
}
