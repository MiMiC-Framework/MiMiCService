#include <gtest/gtest.h>
#include <MService/NetServ/RequestParsing.h>

#include <variant>
#include <iostream>
#include <string>
#include <type_traits>
#include <iomanip>

TEST(RequestParsing, RemoveSubStrOccurrences) {
    std::string str = "Hello \n\nNothing \n";
    std::string substr = "\n";

    replace(str, substr);

    std::string res = "Hello Nothing ";

    ASSERT_EQ(res, str);
}

template<class T> struct always_false : std::false_type {};

TEST(RequestParsing, ParsingAppConfigJsonPayload) {
    // a JSON text
    std::string text = R"(
    {
        "Apps": [
            {
                "AppInfo": {
                    "Name": "Notepad.exe",
                    "Path": "C:\\System32\\notepad.exe"
                },
                "Settings": [
                    {
                        "Name": "Font Size",
                        "ExMode": "Direct",
                        "UIMode": {
                            "Name": "Edit",
                            "Id": "Edit"
                        },
                        "Actions": [
                            {
                                "ElemId": "Format",
                                "Action": "Expand"
                            },
                            {
                                "ElemId": "Accept",
                                "Action": "Invoke"
                            }
                        ]
                    }
                ]
            }
        ]
    }
    )";

    auto pRes = parseRequest(text);

    pMatch(pRes,
        [](auto& requests) {
            for (auto request : requests) {
                std::visit(
                    [](auto&& arg) -> void {
                        using T = std::decay_t<decltype(arg)>;
                        if constexpr (std::is_same_v<T,Vector<AppUISettings>>) {
                            std::cout << std::setw(4) << json { arg[0] } << '\n';
                        } else if constexpr (std::is_same_v<T, AppProcAction>) {
                            FAIL() << "Incorrect parsing result";
                        } else if constexpr (std::is_same_v<T, ServiceAction>) {
                            FAIL() << "Incorrect parsing result";
                        } else {
                            static_assert(always_false<T>::value, "non-exhaustive visitor!");
                        }
                    },
                    request
                );
            }

            ASSERT_FALSE(requests.empty());
        },
        [](auto err){
            FAIL() << err.msg;
        }
    );
}
