#include <MService/UIServ/UIService.h>
#include <MService/LogServ/LogService.h>
#include <MService/Error.h>

#include <FUtils/Sync/SVar.h>
#include <UIHandler/UI/Handler.h>

#include <string>
#include <codecvt>

using UniqueLock = std::unique_lock<std::mutex>;

/**
 * @brief
 *  Execute a action in the UI of a running application.
 * @param
 *  The action to be executed.
 * @return
 *  E_FN_SUCESS or one of the following errors:
 *      - E_FN_INVAL: If parameter was an action that could not be executed right now.
 *      - E_GV_SYS_WIN32ERROR: If error was system related. Use GetLastError to get more information
 *                             about it.
 */
errno_t executeUIAction(const AppInfo& appInfo, const UIAction& uiAction, SVar<LogQueue>& sLogQueue) {
    IUIAutomationElement* window = NULL;
    // TODO: Find window and execute one UI Action
    errno_t fwerr = findWindow(appInfo, &window);
    // TODO: Check the values that can be returned from findwindow.
    if (fwerr != E_FN_SUCCESS) { return fwerr; }

    auto tbnerr = triggerElementByName(uiAction.elemId, window, uiAction.elemAction);
    // Log action result
    // ====================================================================
    updSVar(sLogQueue, [&uiAction, &tbnerr] (LogQueue& queue) {
        // wstring to utf-8 based string converter.
        std::wstring_convert<std::codecvt_utf8<wchar_t>> toUTF8String;

        // Create Error Message
        // ========================================================
        std::string errMsg = "executeUIAction: Executing requested action '";
        errMsg += toString(uiAction.elemAction.code) + "' ";
        errMsg += "over element '" + toUTF8String.to_bytes(uiAction.elemId) + "' ";
        errMsg += "failed with error '" + toString(tbnerr) + "'";
        // ========================================================

        queue.push(errMsg);
    });

    if (tbnerr != E_FN_SUCCESS) { return tbnerr; }

    return E_FN_SUCCESS;
}

/**
 * @brief
 * @param elemAction
 * @return
 */
std::string toString(UIAction elemAction) {
    std::string eventStr = "";
    auto type = elemAction.elemAction.code;

    if (type == Action_Invoke) {
        return "Action_Invoke";
    } else if (type == Action_ToogleExpandCollapse) {
        return "Action_ToogleExpandCollapse";
    } else if (type == Action_Expand) {
        return "Action_Expand";
    } else if (type == Action_Collapse) {
        return "Action_Collapse";
    } else if (type == Action_Write) {
        return "Action_Write";
    } else if (type == Action_Select) {
        return "Action_Select";
    } else {
        return "Unspecified Action";
    }
}

// TODO: Process windows events function.
// ============================================================================
// Should handle the events pushed by the UI Events handlers and trigger the
// registered actions associated with those events.
// TODO:
// Define the API for registered actions for events. For now this API is simply
// taking into account the mode in which the action should be triggered.
// ============================================================================
/**
 * @brief
 *  Process the new received event.
 * @param regAppEventActions
 *  List of current registered events for searching and matching the new event.
 * @param event
 *  The new triggered event to be processed.
 * @param sLogQueue
 *  The queue of the logging service, to push new messages.
 * @return
 *   E_FN_SUCESS or one of the following errors:
 *       - E_FN_INVAL: If the new event to be processed was not registered.
 */
errno_t processEvent(
    Vector<AppInfo>&    regApps,
    Vector<UIAction>&   regAppActions,
    UIEvent&            event,
    SVar<LogQueue>&     sLogQueue
) {
    for (UIAction curAction : regAppActions) {
        if (curAction.appId == event.appId && curAction.elemId == event.elemId) {
            auto appIt =
                std::find_if(
                    regApps.begin(),
                    regApps.end(),
                    [&curAction](auto app)  -> bool {
                        return app.appId == curAction.appId;
                    }
                );

            if (appIt == regApps.end()) {
                return E_FN_INVAL;
            }

            auto exerr = executeUIAction(*appIt, curAction, sLogQueue);
            // Send Error to logging service
            // ================================================================
            if (exerr != E_FN_SUCCESS) {
                updSVar(sLogQueue, [&exerr, &curAction] (LogQueue& queue) {
                    // wstring to utf-8 based string converter.
                    std::wstring_convert<std::codecvt_utf8<wchar_t>> toUTF8String;

                    // Create Error Message
                    // ========================================================
                    std::string errMsg = "processEvent: Error, requested action '";
                    errMsg += toString(curAction) + "' ";
                    errMsg += "over element '" + toUTF8String.to_bytes(curAction.elemId) + "' ";
                    errMsg += "failed with error '" + toString(exerr) + "'";
                    // ========================================================

                    queue.push(errMsg);
                });
            }
            return exerr;
            // ================================================================
        }
    }
    return E_FN_INVAL;
}

// TODO: Process UI Action requests function
bool checkFlags(const std::vector<bool*>& flags) {
    for(auto flag : flags) {
        if (*flag == true) { return true; }
    }
    return false;
}

void _startUIService(
    SyncRsc<UIEventQueue>&          uiEventsRes,
    SyncRsc<AppUISettingsQueue>&    appUISettingsRes,
    SVar<LogQueue>&                 sLogQueue,
    SVar<bool>&                     sStopFlag
) {
    // Flags for handling the different event sources the service should deal with.
    Vector<bool*> flags {
        &uiEventsRes.flag,
        &appUISettingsRes.flag
    };

    // Registered applications and its actions.
    Vector<AppInfo>  regApps {};
    Vector<UIAction> regActions {};

    //while (!readSVar(sStopFlag)) {
    //    Lock<UIEventQueue> uiEventQLock(uiEventsRes.m);
    //    wait(uiEventsRes.cv, uiEventQLock, [&flags] () { return checkFlags(flags); });
//
    //    if (uiEventsRes.flag == true) {
    //        // Swap queue values
    //        // ====================================================================
    //        UIEventQueue newEvents {};
    //        (*uiEventQLock).swap(newEvents);
//
    //        // Unlock the Mutex
    //        // ====================================================================
    //        uiEventQLock.unlock();
    //        uiEventsRes.cv.notify_all();
//
    //        // Process and log windows events
    //        // ====================================================================
    //        while(!newEvents.empty()) {
    //            auto newEvent = std::move(newEvents.front());
    //            newEvents.pop();
    //            processEvent(
    //                regApps,
    //                regActions,
    //                newEvent,
    //                sLogQueue
    //            );
    //        }
    //    } else {
    //        Lock<AppUISettingsQueue> appUISettingsQLock(appUISettingsRes.m);
    //        wait(
    //            appUISettingsRes.cv,
    //            appUISettingsQLock,
    //            [&appUISettingsRes] { return appUISettingsRes.flag; }
    //        );
//
    //        // Swap queue values
    //        //      AppUISettingsQueue newSettings {};
    //        (*appUISettingsQLock).swap(newSettings);
//
    //        // Process and log UI Actions requests
    //        // ====================================================================
    //        while(!newSettings.empty()) {
    //            // Get first received new AppSetting and remove from list.
    //            auto newSetting = std::move(newSettings.front());
    //            newSettings.pop();
//
    //            // Search UI action in registered events and execute.
    //            // executeUIAction(
    //            //     newAction,
    //            //     sLogQueue
    //            // );
    //        }
    //    }
    //}
}

errno_t startUIService(
    SyncRsc<UIEventQueue>&          uiEventsQueue,
    SyncRsc<AppUISettingsQueue>&    appUISettingsQueue,
    SVar<LogQueue>&                 sLogQueue,
    SVar<bool>&                     sStopFlag,
    std::thread&                    uiServiceThread
) {
    errno_t error = E_FN_SUCCESS;

    try {
        //uiServiceThread =
        //    std::thread(
        //        _startUIService,
        //        CRef(uiEventsQueue),
        //        CRef(appUISettingsQueue),
        //        CRef(sLogQueue),
        //        CRef(sStopFlag)
        //    );
    } catch (std::system_error err) {
        return error = err.code().value();
    }

    return error;
}
