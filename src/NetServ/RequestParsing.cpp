#include <UIHandler/UI/Handler.h>

#include <MService/NetServ/RequestParsing.h>

#include <codecvt>

using json = nlohmann::json;

void replace(std::string& str, std::string& subStr) {
    std::string::size_type n = 0;

    if (subStr.back() == '\0') {
        subStr.pop_back();
    }

    while( (n = str.find(subStr)) != std::string::npos) {
        str.erase(n, subStr.length());
    }
}

UIElemAction parseAction(std::string action, std::wstring payload) {
    if (action == "Invoke") {
        return UIElemAction { Action_Invoke, Variant {VT_PVoid, NULL}};
    } else if (action == "Expand") {
        return UIElemAction { Action_Expand, Variant {VT_PVoid, NULL}};
    } else if (action == "Collapse") {
        return UIElemAction { Action_Collapse, Variant {VT_PVoid, NULL}};
    } else if (action == "Write") {
        size_t plSize = payload.length() + 1;
        wchar_t* plData = (wchar_t*)malloc(plSize);
        wcscpy(plData, payload.c_str());

        Variant vPl;
        vPl.type  = VT_CArray;
        vPl.pVoid = (void*)plData;

        return UIElemAction { Action_Write, vPl };
    } else {
        std::string err = "Error at 'parseAction': unknown action: " + action;
        throw std::invalid_argument(err);
    }
}

std::vector<Setting> parseSettings(json jSettings) {
    std::vector<Setting> settings;

    for (auto jSetting : jSettings) {
        Setting setting  = jSetting;
        settings.push_back(setting);
    }

    return settings;
}

Request parseAppSettings(json jApps) {
    std::vector<AppUISettings> appUISettings;

    uint32_t count = 0;

    for (auto& jApp : jApps) {
        try {
            AppInfo appInfo = jApp.at("AppInfo");
            auto jSettings = jApp.at("Settings");

            std::vector<Setting> uiSettings = parseSettings(jSettings);
            appUISettings.push_back({ appInfo, uiSettings });

            if (count != UINT32_MAX) {
                count += 1;
            }
        } catch (std::exception& ex){
            throw std::invalid_argument(
                std::string("Error parsing App number - '") + std::to_string(count) + "' : " +
                ex.what()
            );
        }
    }

    return appUISettings;
}

Either<Vector<Request>,ParseError> parseRequest(std::string requests) {
    try {
        auto jRequests = json::parse(requests.begin(), requests.end());
        Vector<Request> requests;

        for (auto jRequest = jRequests.begin(); jRequest != jRequests.end(); ++jRequest) {
            if (jRequest.key() == "Apps") {
                requests.push_back(parseAppSettings(jRequest.value()));
            } else if (jRequest.key() == "ServiceAction") {
//              requests.push_back(parseServAction(jRequest));
            } else if (jRequest.key() == "UserAction") {
//              requests.push_back(parse);
            } else {
            }
        }

        return requests;
    } catch (std::invalid_argument& ex) {
        return ParseError { E_FN_INVAL, std::string(ex.what()) };
    }
}
