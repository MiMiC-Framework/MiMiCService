#include <MService/NetServ/NetworkService.h>
#include <MService/NetServ/RequestParsing.h>
#include <MService/LogServ/LogService.h>
#include <MService/Payloads/AppUISettings.h>

#include <FUtils/Sync/SVar.h>
#include <FUtils/Sync/MSVar.h>

#include <FUtils/Iter/While.h>

#include <Std/Utility.h>
#include <Std/Functional.h>

#include <string>
#include <thread>
#include <atomic>

using Socket = asio::ip::tcp::socket;

Either<std::string, asio::error_code> readline(Socket& socket) {
    asio::streambuf buf;
    asio::error_code error;
    asio::read_until(socket, buf, "\x03", error);

    if (!error) {
        auto rawData = asio::buffer_cast<const char*>(buf.data());
        std::string data;

        if (*rawData != '\0') {
            data = asio::buffer_cast<const char*>(buf.data());
        } else {
            data = "";
        }

        // This is just in case delimiter is unwanted from the String.
        data.erase( --data.end() );

        return data;
    } else {
        auto msg = error.message();
        return error;
    }
}

asio::error_code sendline(Socket& socket, const std::string& str ) {
    const std::string msg = str + "\n";
    try {
        asio::write( socket, asio::buffer(msg) );
    } catch (asio::system_error ex) {
        return ex.code();
    }

    return asio::error_code();
}

bool isErrorOrShutdown(Either<ServiceActionCode, asio::error_code> msg) {
    auto res =
        pMatch(
            msg,
            [] (auto action) {
                if (action == ServAction_Stop) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            },
            [] (auto err) {
                if (err == asio::error::eof) {
                    return TRUE;
                } else if (err == asio::error::broken_pipe) {
                    return TRUE;
                } else if (err == asio::error::connection_aborted) {
                    return TRUE;
                } else if (err == asio::error::connection_aborted) {
                    return TRUE;
                } else if (err == asio::error::interrupted) {
                    return TRUE;
                } else {
                    // If unknown error, no shutdown.
                    return FALSE;
                }
            }
       );

    return res;
}

using StopFlag = std::pair<std::condition_variable&, std::mutex&>;

/**
 * @brief
 *  Process new request.
 * @param socket
 *  Socket to be used for receiving new package.
 * @param pSync
 *  Synchronization primitives for informing when new packages arrives
 *
 * @return An error if the handling has went wrong, or the new received Request.
 */
auto procNewRequest(
    std::shared_ptr<Socket> sock,
    SVar<RequestQueue>&     requestQueue
) -> Either<ServiceActionCode, asio::error_code>
{
    using ProcRes = Either<ServiceActionCode, std::error_code>;

    auto& socket = (*sock);
    auto rawMsg = readline(socket);

    ProcRes res =
        pMatch(rawMsg,
            [&socket,&requestQueue](auto str) {
                auto request = parseRequest(str);

                ProcRes sAction =
                    pMatch(request,
                        [&socket,&requestQueue](auto&& newRequests) -> ProcRes {
                            updSVar(
                                requestQueue,
                                [newRequests = move(newRequests)](auto& queue) {
                                    for (auto request : newRequests) {
                                        queue.push(request);
                                    }
                                }
                            );

                            std::cout << "Sending Okay response." << "\r\n";
                            sendline(socket, "Received new valid AppSettings");

                            return ProcRes(ServAction_KeepAlive);
                        },
                        [&socket](auto&& err) -> ProcRes {
                            std::cout << "Sending Error response." << "\r\n";
                            std::string invSchemaMsg = "Incorrect App Setting schema: ";
                            auto errorCodeMsg = "Error Code: " + std::to_string(err.code) ;
                            auto errorSpecMsg = ", with error message \"" + err.msg + "\"";

                            auto errorMsg = invSchemaMsg + errorCodeMsg + errorSpecMsg;

                            sendline(socket, errorMsg);

                            return ProcRes(ServAction_KeepAlive);
                        }
                    );

                return sAction;
            },
            [](auto err) {
                return ProcRes(err);
            }
        );

    return res;
}

template <typename T>
using SharedPtr = std::shared_ptr<T>;

/**
 * @brief
 *  Receives operations payloads, and notify to the main thread a new
 *  operation is pending.
 */
void tcpReceiver(
    const asio::error_code&     err,
    std::shared_ptr<Socket>     socket,
    asio::ip::tcp::acceptor&    acceptor,
    SVar<RequestQueue>&         sReqQueue,
    SVar<LogQueue>&             sLogQueue,
    SVar<bool>&                 sStopFlag
) {
    using ProcRes = Either<ServiceActionCode, std::error_code>;

    auto nativeHandle = (*socket).native_handle();
    std::cout << "New connection stablished in socket: " << nativeHandle << "\r\n" ;

    auto res =
        While(
            ServAction_KeepAlive,
            Fn<bool(ProcRes)>(isErrorOrShutdown),
            Fn<ProcRes(SharedPtr<Socket>&, SVar<RequestQueue>&)>(procNewRequest),
            socket,
            sReqQueue
        );

    std::cout << "Closing socket: " << nativeHandle << "\r\n";
    (*socket).shutdown(asio::socket_base::shutdown_both);
    (*socket).close();

    std::shared_ptr<Socket> sock(new Socket(acceptor.get_io_service()));

    std::function <void(const asio::error_code&)> acceptHandler =
        std::bind(
            tcpReceiver,
            std::placeholders::_1,
            sock,
            Ref(acceptor),
            Ref(sReqQueue),
            Ref(sLogQueue),
            Ref(sStopFlag)
        );

    acceptor.async_accept(
        *sock,
        acceptHandler
    );
}

errno_t startNetService(
    uint16_t            port,
    SVar<RequestQueue>& sReqQueue,
    SVar<LogQueue>&     sLogQueue,
    SVar<bool>&         sStopFlag,
    std::thread*        netServThread,
    uint16_t*           freePort
) {
    // Thread: NetService own thread, continuosly accepting new connections for
    // requests.
    // ========================================================================
    std::thread _netServiceThread(
        [port, &sReqQueue, &sLogQueue, &sStopFlag, &freePort] () {
            // Try to init to create a io_service and assign and endpoint to it.
            // =================================================================
            asio::io_service io_service;
            asio::error_code endpointErr;

            asio::ip::tcp::acceptor acceptor(
                io_service,
                asio::ip::tcp::endpoint(asio::ip::tcp::v4(), port),
                &endpointErr
            );

            if (endpointErr) {
                auto errVal = endpointErr.value();

                if (errVal == asio::error::address_in_use && freePort != NULL) {
                    acceptor =
                        asio::ip::tcp::acceptor(
                            io_service,
                            asio::ip::tcp::endpoint(asio::ip::tcp::v4(), 0),
                            &endpointErr
                        );

                    if (endpointErr) {
                        // TODO: Map the correct error value back.
                        return endpointErr.value();
                    }

                    *freePort = acceptor.local_endpoint().port();
                }
            }

            std::shared_ptr<Socket> sock(new Socket(acceptor.get_io_service()));

            std::function <void(const asio::error_code&)> acceptHandler =
                std::bind(
                    tcpReceiver,
                    std::placeholders::_1,
                    sock,
                    Ref(acceptor),
                    Ref(sReqQueue),
                    Ref(sLogQueue),
                    Ref(sStopFlag)
                );

            acceptor.async_accept(
                *sock,
                acceptHandler
            );

            io_service.post([&sStopFlag, &io_service] () {
                std::thread waiter([&sStopFlag, &io_service] () {
                    while (!readSVar(sStopFlag));
                    io_service.stop();
                });
                waiter.detach();
            });

            io_service.run();
        }
    );

    // Communicate back the thread for latter handling.
    // ========================================================================
    *netServThread = std::move(_netServiceThread);

    return E_FN_SUCCESS;
}
