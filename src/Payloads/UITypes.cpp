#include <MService/Payloads/AppUISettings.h>
#include <UIHandler/UI/UITypes.h>

#include <codecvt>

// AppInfo related functions
// ============================================================================
void to_json(json& j, const AppInfo& appInfo) {
    j = json { "AppInfo", {{"Name", appInfo.name}, {"Path", appInfo.path}} };
}

void from_json(const json& j, AppInfo& appInfo) {
    try {
        appInfo.name = j.at("Name").get<std::string>();
        auto path = j.at("Path").get<std::string>();
        appInfo.path.assign(path.begin(), path.end());
    } catch (std::exception& ex) {
        throw std::invalid_argument(
            std::string("-> AppInfo: ") + ex.what()
        );
    }
}

bool operator==(const AppInfo& app1, const AppInfo& app2) {
    return (app1.name == app2.name) && (app1.path == app2.path);
}
// ============================================================================

// AppUIMode related functions
// ============================================================================
bool operator==(const AppUIMode& appMode1, const AppUIMode& appMode2) {
    bool sameName = appMode1.name == appMode2.name;
    bool sameModeId = appMode1.id == appMode2.id;

    return (sameName && sameModeId);
}

void to_json(json& j, const AppUIMode& appUIMode) {
    j = json { "UIMode", {{"Name", appUIMode.name}, {"Id", appUIMode.id}} };
}

void from_json(const json& j, AppUIMode& appUIMode) {
    try {
        appUIMode.name  = j.at("Name").get<std::string>();
        appUIMode.id    = j.at("Id").get<std::string>();
    } catch(std::exception& ex) {
        throw std::invalid_argument(
            std::string("-> AppUIMode: ") + ex.what()
        );
    }
}

// ============================================================================

// UIElemAction related functions
// ============================================================================
bool operator==(const UIElemAction& uiElemA1, const UIElemAction& uiElemA2) {
    bool sameCode = uiElemA1.code == uiElemA2.code;
    if (uiElemA1.code == Action_Invoke) {
        return sameCode;
    } else if (uiElemA1.code == Action_Expand) {
        return sameCode;
    } else if (uiElemA1.code == Action_Collapse) {
        return sameCode;
    } else if (uiElemA1.code == Action_ToogleExpandCollapse) {
        return sameCode;
    } else if (uiElemA1.code == Action_Write) {
        // TODO: Improve variant type to receive normal and wide chars. (Exploit with confusion)
        bool sameVTypes = uiElemA1.pl.type == uiElemA2.pl.type;
        bool strVType = uiElemA1.pl.type == VT_PChar;

        if (sameVTypes && strVType) {
            return strcmp(uiElemA1.pl.pChar, uiElemA2.pl.pChar);
        }
    } else if (uiElemA1.code == Action_Select) {
        bool sameVTypes = uiElemA1.pl.type == uiElemA2.pl.type;

        bool numSelec = uiElemA1.pl.type == VT_Int16;
        if (sameVTypes && numSelec) {
            return uiElemA1.pl.i16Val == uiElemA2.pl.i16Val;
        }

        bool idSelec = uiElemA1.pl.type == VT_PChar;
        if (sameVTypes && idSelec) {
            return strcmp(uiElemA1.pl.pChar, uiElemA2.pl.pChar);
        }

        return false;
    }

    return false;
}

UIElemActionCode parseUIElemActionCode(std::string s) {
    if (s == "Invoke") {
        return UIElemActionCode::Action_Invoke;
    } else if (s == "Collapse") {
        return UIElemActionCode::Action_Collapse;
    } else if (s == "Expand") {
        return UIElemActionCode::Action_Expand;
    } else {
        return UIElemActionCode::Action_ENODEF;
    }
}

std::string toString(UIElemActionCode code) {
    if (code == UIElemActionCode::Action_Invoke) {
        return "Action_Invoke";
    } else if (code == UIElemActionCode::Action_Collapse) {
        return "Action_Collapse";
    } else if (code == UIElemActionCode::Action_ToogleExpandCollapse) {
        return "Action_ToogleExpandCollapse";
    } else if (code == UIElemActionCode::Action_Write) {
        return "Action_Write";
    } else if (code == UIElemActionCode::Action_Select) {
        return "Action_Select";
    } else {
        return "Action_ENODEF";
    }
}
// ============================================================================

// ============================================================================

// UIAction related functions
// ============================================================================
bool operator==(const UIAction& appUIElemA1, const UIAction& appUIElemA2){
    bool sameAppInfo = appUIElemA1.appId == appUIElemA2.appId;
    bool sameElemId  = appUIElemA1.elemId == appUIElemA2.elemId;
    bool sameAction  = appUIElemA1.elemAction == appUIElemA2.elemAction;

    return (sameAppInfo && sameElemId && sameAction);
}

void from_json(const json& j, UIAction& action) {
    // wstring to utf-8 based string converter.
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> toUTF16;

    try {
        action.elemId  = toUTF16.from_bytes( j.at("ElemId").get<std::string>() );
        // TODO: Parse Actions with payloads, no just simple actions.
        action.elemAction = UIElemAction {};
        action.elemAction.code = parseUIElemActionCode( j.at("Action").get<std::string>() );
    } catch(std::exception& ex) {
        throw std::invalid_argument(
            std::string("-> UIAction: ") + ex.what()
        );
    }
}

void from_json(const json &j, Vector<UIAction>& uiActions) {
    for (const json& jAction : j) {
        uiActions.push_back(jAction);
    }
}

void to_json(json& j, const UIAction& action) {
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> toUTF8;
    j = json { {"ElemId", toUTF8.to_bytes(action.elemId)},
               {"Action", toString(action.elemAction.code) } };
}

// ============================================================================

// Setting related functions
// ============================================================================
bool operator==(const Setting& setting1, const Setting& setting2) {
    bool sameName = setting1.name == setting2.name;
    bool sameMode = setting1.exMode == setting2.exMode;
    bool sameActions = setting1.actions == setting2.actions;

    bool sameUIMode = true;

    if (sameMode && setting1.exMode == SettingExMode::SetExMode_Event) {
        bool nempty = setting1.uiMode && setting2.uiMode;
        if (nempty) {
           sameUIMode = *(setting1.uiMode) == *(setting2.uiMode);
        }
    }

    return (sameName && sameMode && sameActions && sameUIMode);
}

std::string toString(SettingExMode exMode) {
    if (exMode == SettingExMode::SetExMode_Direct) {
        return "Direct";
    } else if (exMode == SettingExMode::SetExMode_Event) {
        return "Event";
    } else {
        return "ENODEF";
    }
}

SettingExMode parseSettingExMode(std::string s) {
    if (s == "Direct") {
        return SettingExMode::SetExMode_Direct;
    } else if (s == "Event") {
        return SettingExMode::SetExMode_Event;
    } else {
        return SettingExMode::SetExMode_ENODEF;
    }
}

void from_json(const json& j, Setting& setting) {
    try {
        // Should be present or failure.
        // ========================================================================
        try {
            setting.name = j.at("Name").get<std::string>();
        } catch(std::exception& ex) {
            throw std::invalid_argument(
                std::string("-> Name: ") + ex.what()
            );
        }

        auto jActions = j.at("Actions");
        // ========================================================================

        // Optionals.
        // ========================================================================
        setting.exMode = SettingExMode::SetExMode_Direct;
        // ========================================================================

        for (const json& jAction : jActions) {
            setting.actions.push_back(jAction);
        }

        for (auto jSettingField = j.begin(); jSettingField != j.end(); ++jSettingField) {
            if (jSettingField.key() == "ExMode") {
                try {
                    setting.exMode  = parseSettingExMode(jSettingField.value().get<std::string>());
                } catch (std::exception& ex) {
                     throw std::invalid_argument(
                         std::string("-> ExMode: ") + ex.what()
                     );
                }
            }
            if (jSettingField.key() == "UIMode") {
                try {
                    // Be careful in using the overloaded assigment operator to initialize from JSON.
                    setting.uiMode = jSettingField.value();
                } catch (std::exception& ex) {
                     throw std::invalid_argument(
                         std::string("-> UIMode: ") + ex.what()
                     );
                }
            }
        }
    } catch (std::exception& ex) {
        throw std::invalid_argument(
            std::string(" -> Setting ") + ex.what()
        );
    }
}

void to_json(json& j, const Setting& setting) {
    if (setting.uiMode) {
        j = { {"Name", setting.name},
              {"ExMode", toString(setting.exMode)},
              {"UIMode", {{"Name", (*setting.uiMode).name}, {"Id", (*setting.uiMode).id}}},
              {"Actions", json(setting.actions)}
            };
    } else {
        j = { {"Name", setting.name},
              {"ExMode", toString(setting.exMode)},
              {"Actions", json(setting.actions)}
            };
    }
}

void to_json(json& j, const Vector<Setting>& settings) {
    json jSettings;
    for (auto& setting : settings) {
        jSettings.push_back(setting);
    }

    j  = { "Settings", { jSettings } };
}

// ============================================================================

// AppUISettings related functions
// ============================================================================
bool operator==(const AppUISettings& app1, const AppUISettings& app2) {
    bool sameAppInfo  = app1.appInfo == app2.appInfo;
    bool sameSettings = app1.settings == app2.settings;

    return (sameAppInfo && sameSettings);
}

void to_json(json& j, const AppUISettings& uiSettings) {
    j = json { "AppUISettings",
                   { {"AppInfo", uiSettings.appInfo},
                     uiSettings.settings }
             };
}

void from_json(const json& j, AppUISettings& uiSettings) {
    try {
        uiSettings.appInfo  = j.at("AppInfo");

        for (const json& setting : j.at("Settings")) {
            uiSettings.settings.push_back(setting);
        }
    } catch(std::exception& ex) {
        throw std::invalid_argument(
            std::string("from_json -> AppUISettings ") + ex.what()
        );
    }
}

// ============================================================================
