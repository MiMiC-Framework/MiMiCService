// Networking
#define ASIO_STANDALONE
#define ASIO_HAS_STD_ADDRESSOF
#define ASIO_HAS_STD_ARRAY
#define ASIO_HAS_CSTDINT
#define ASIO_HAS_STD_SHARED_PTR
#define ASIO_HAS_STD_TYPE_TRAITS

// Networking
#include <asio.hpp>
// After including asio we need to specifically import interface defs.
#include <Ole2.h>

// Threading
#include "FUtils/Sync/WMutex.h"
#include "Std/CV.h"

#include <atomic>
#include <thread>

// Operations
#include <utility>
#include <algorithm>
#include <queue>

// Utilities
#include <string>
#include <stdint.h>

// For user info
#include <iostream>

// Logging (own)
#include <MService/LogServ/LogService.h>

// Networking (own)
#include <MService/NetServ/NetworkService.h>

// UI (own)
#include <MService/UIServ/UIService.h>

// Payload types
#include <MService/Payloads/AppUISettings.h>

// Concurrency (own)
#include <FUtils/Sync/SVar.h>

// Utilities
#include <Std/Vector.h>

int main(int argc, char* argv[]) {
    // Stop flag for cleanly exiting all service threads.
    // ======================================================================
    SVar<bool> sStopFlag { false };

    // Start LogService
    // ======================================================================
    SVar<LogQueue>  sLogQueue { LogQueue {} };
    std::thread     logServThread;

    //  In the future a interface for different loggers will be provided.
    //  std::list<logInterfaces> loggers;

    startLogService(sLogQueue, sStopFlag, logServThread);
    // ======================================================================

    // Start UI Interface Service
    // ======================================================================

    //                      UIEvents Resources
    // ======================================================================
    WMutex<UIEventQueue> uiEventQMutex { UIEventQueue {} };
    CV uiEventQCV;
    bool uiEventQFlag;

    SyncRsc<UIEventQueue> uiEventsRes {
        uiEventQMutex,
        uiEventQCV,
        uiEventQFlag
    };
    // ======================================================================

    //                    UIElemActions Resources
    // ======================================================================
    WMutex<AppUISettingsQueue> appUISettingsQMutex { AppUISettingsQueue {} };
    CV appUISettingsQCV;
    bool appUISettingsQFlag;

    SyncRsc<AppUISettingsQueue> appUISettingsRes {
        appUISettingsQMutex,
        appUISettingsQCV,
        appUISettingsQFlag
    };
    // ======================================================================

    std::thread uiServThread;

    startUIService(
        uiEventsRes,
        appUISettingsRes,
        sLogQueue,
        sStopFlag,
        uiServThread
    );

    // Start NetService
    // ======================================================================
    uint16_t           assignedPort;
    // std::thread        netServThread;
    SVar<RequestQueue> sReqQueue { RequestQueue{} };
    // SRVar<AppUIElemActionQueue> sUIElemActionQueue { uiServiceSyncPs, uiElemActionFlag, appUIElemActionQueue };

    // startNetService(31400, sReqQueue, sLogQueue, sStopFlag, &netServThread, &assignedPort);
    auto errMsg = std::string("NetService: Assigned port is ") + std::to_string(assignedPort);

    // updSVar(sLogQueue, [&errMsg](auto queue) { queue.push(errMsg); });

    // Main thread should have a mechanism to update services and to ensure that
    // they are all working and restart them in case of internal, unexpected failure.
    while (!readSVar(sStopFlag)) {

    }

    // Wait for all services threads to finish before exiting main thread.
    // ======================================================================
    logServThread.join();
    uiServThread.join();
    // netServThread.join();

    return 0;
}
