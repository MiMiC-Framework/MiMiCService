from conans import ConanFile, CMake, tools
import os

class MiMiCServiceConan(ConanFile):
    name = "MiMiCService"
    version = "0.1"
    license = "Raising The Floor"
    url = "https://gitlab.com/MiMiC-Framework/MiMiCService"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "tests": [True, False]}
    generators = "cmake"
    requires = (
        "gtest/1.8.0@bincrafters/stable",
        "UErrno/0.1@philabs/testing",
        "CArray/0.1@philabs/testing",
        "CVariant/0.1@philabs/testing",
        "UIHandler/0.1@philabs/testing",
        "FUtils/0.1@philabs/testing",
        "StdWrapper/0.1@philabs/testing",
        "asio/1.10.8@lasote/vcpkg",
        "jsonformoderncpp/2.1.1@vthiery/stable"
    )
    default_options = (
        "shared=False",
        "tests=False",
        #  GTest options
        "gtest:shared=False",
        "gtest:build_gmock=False"
    )

    def source(self):
        self.run("git clone https://gitlab.com/MiMiC-Framework/MiMiCService")
        self.run("cd MiMiCService && git checkout develop")

    def build(self):
        cmake = CMake(self, parallel=True)
        if self.options.tests:
            cmake.definitions["CMAKE_BUILD_TESTS"] = "ON"
        cmake.configure(source_folder="MiMiCService")
        cmake.build(target="install")

    def imports(self):
       self.copy("*.dll", "bin")

    def package(self):
        self.copy("*.h", dst="include", src="MService/include")
        self.copy("*Service.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="lib", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["MService"]
